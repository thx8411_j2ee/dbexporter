INFORMATIONS SUR LES DRIVERS JDBC INTÉGRÉS:
###########################################

DRIVER ORACLE:
______________________________
Version      |Jdbc oracle 12c |
_____________|________________|
Compatibilité|oracle-12.x.0.x |
             |oracle-11.x.0.x |
_____________|________________|

DRIVER SYBASE:
______________________________
Version      |JTDS-1.3.1      |
             |Sybase Driver   |
_____________|________________|
Compatibilité|sybase-15.0     |
             |sybase-12.5.4   |
             |sybase-12.5.3   |
             |sybase-12.5.2   |
             |sybase-12.5.1   |
             |sybase-12.5     |
             |sybase-12.0     |
             |sybase-11.9     |
             |sybase-11.5     |
_____________|________________|

DRIVER MYSQL:
______________________________
Version      |Jdbc mysql 5.1  |
_____________|________________|
Compatibilité|mysql-5.7       |
             |mysql-5.6       |
             |mysql-5.5       |
             |mysql-5.1       |
             |mysql-5.0       |
             |mysql-4.1       |
_____________|________________|

DRIVER SQL-SERVER:
______________________________
Version      |Jdbc Sql-Server |
             |6.2             |
_____________|________________|
Compatibilité|sqlserver-2008  |
             |sqlserver-2008R2|
             |sqlserver-2012  |
             |sqlserver-Azure |
             |sqlserver-2014  |
             |sqlserver-2016  |
             |sqlserver-2017  |
_____________|________________|

DRIVER POSTGRESQL:
______________________________
Version      |Jdbc postgresql |
             |42.2.6          |
_____________|________________|
Compatibilité|à partir de 8.2 |
_____________|________________|


