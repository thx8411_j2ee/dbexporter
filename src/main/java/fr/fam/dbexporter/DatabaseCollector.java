
package fr.fam.dbexporter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import fr.fam.dbexporter.config.DatabaseConfig;
import fr.fam.dbexporter.config.Applications;
import fr.fam.dbexporter.config.Application;
import fr.fam.dbexporter.config.Source;
import io.prometheus.client.Collector;
import io.prometheus.client.GaugeMetricFamily;

/**
*
*/
public class DatabaseCollector extends Collector {

    /** full scraping timeout (in seconds). */
    static final long COLLECTOR_TIMEOUT = 10L;

    /** */
    private static final Logger LOGGER = Logger.getLogger(DatabaseCollector.class);

    /** */
    private Set<DatabaseScraper> scrapers = new HashSet<DatabaseScraper>();

    /** */
    private DatabaseConfig config;

    /** */
    private Applications applications;

    /** threads pool. */
    private ExecutorService scrapPool;

    /**
    *
    * @param pconfig configuration
    */
    public DatabaseCollector(final DatabaseConfig pconfig) {
        super();
        config = pconfig;
        applications = config.getApplications();

        // populate scrapers for each source
        // for each app
        LOGGER.debug("Creating scrapers...");
        for (Application application : applications.getApplications()) {

            // for each source
            for (Source source : application.getSources() ) {

                // create and add scraper

                scrapers.add(new DatabaseScraper(source));
            }
        }
        LOGGER.debug("Done.");

        // create the thread pool
        scrapPool = Executors.newFixedThreadPool(scrapers.size());
    }

    /**
    *
    */
    public final void close() {
        scrapPool.shutdownNow();
    }

    /**
    *
    * @throws IllegalStateException
    */
    @Override
    public final List<MetricFamilySamples> collect() throws IllegalStateException {
        LOGGER.debug("Building samples...");

        List<MetricFamilySamples> mfs = new ArrayList<MetricFamilySamples>();

        // scrap sources
        // scraping is multithreaded
        // we scrap by sources using sources.getMetrics(), so it's thread safe
        // we use invokeAll to wait for all tasks to finish
        // So, scrapers need to be callable objects
        try {
            scrapPool.invokeAll(scrapers, COLLECTOR_TIMEOUT, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Timeout on source scraping thread pool");
        } catch(IllegalStateException ise) {
        	LOGGER.error("illegal state exception " + ise.toString());
        }

        // label keys list
        List<String> labelKeys = new ArrayList<String>();
        labelKeys.add("application");
        labelKeys.add("source");
        labelKeys.add("env");

        // create gauge
        GaugeMetricFamily gauge = new GaugeMetricFamily("database_probeAvailability", "Help for database_probeAvailability", labelKeys);

        // for each application
        for (Application application : applications.getApplications()) {

            // for each source
            for (Source source : application.getSources()) {

                LOGGER.debug("New source : " + source.getUrl());

                // label values list
                List<String> labelValues = new ArrayList<String>();
                labelValues.add(application.getName());
                labelValues.add(source.getUrl());
                labelValues.add(source.getEnv());

                // populate gauge
                gauge.addMetric(labelValues, source.getValue());

	    }

        }

        mfs.add(gauge);

        return mfs;
    }

}
