package fr.fam.dbexporter;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PageMaker extends HttpServlet{

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
	
	MysqlCon mysql = new MysqlCon();
	mysql.main() ;
	String monInfo = mysql.getInfo();
	String status = mysql.getState();

	response.setContentType("text/html");
	response.setCharacterEncoding( "UTF-8" );
	PrintWriter out = response.getWriter();
	out.println("<!DOCTYPE html>");
	out.println("<html>");
	out.println("<head>");
	out.println("<meta charset=\"utf-8\" />");
	out.println("<title>Test jdbc </title>");
	out.println("</head>");
	out.println("<body>");
	out.println("<p>Voici l'information : "+ monInfo +"</p>");
	out.println("<p>Le status de la DB est : "+ status +"</p>");
	out.println("</body>");
	out.println("</html>");
		
	}



}
