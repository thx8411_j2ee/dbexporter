
package fr.fam.dbexporter;

import java.io.IOException;
import java.io.Writer;
import java.util.Collections;

import javax.servlet.annotation.WebServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import io.prometheus.client.exporter.common.TextFormat;


@WebServlet(name = "MetricsServlet", urlPatterns = {"/metrics"})
public class MetricsServlet extends HttpServlet {

    
    private static final long serialVersionUID = 1L;

    
    private static final Logger LOGGER = Logger.getLogger(MetricsServlet.class);

    /**
    *
    * @param request request
    * @param response response
    * @throws ServletException ServletException
    * @throws IOException IOException
    */
    protected final void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        ServletContext sc = request.getServletContext();
        DatabaseCollector collector = (DatabaseCollector) sc.getAttribute("collector");

        Writer writer = response.getWriter();
        try {
            response.setStatus(HttpServletResponse.SC_OK);
            response.setContentType(TextFormat.CONTENT_TYPE_004);
            TextFormat.write004(writer, Collections.enumeration(collector.collect()));
            writer.flush();
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            LOGGER.error("Failure during database scrap", e);
        } finally {
            writer.close();
        }
    }

}

