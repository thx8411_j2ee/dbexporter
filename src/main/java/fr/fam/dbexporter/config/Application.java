package fr.fam.dbexporter.config;

import java.util.List;
import java.util.ArrayList;

/**
*
*/
public class Application {
    /** */
    private String name;

    /** */
    private List<Source> sources = new ArrayList<Source>() ;

    /**
    *
    * @return name
    */
    public final String getName() {
        return name;
    }

    /**
    *
    * @param pname application name
    */
    public final void setName(final String name) {
        this.name = name;
    }
    
    
    public final List<Source>  getSources() {
        return sources;
    }

    /**
    *
    * @param pname application name
    */
    public final void setSources(final List<Source> sources) {
        this.sources = sources;
    }

    /**
    *
    * @return string
    */
    
    @Override
    public final String toString() {
        StringBuilder s = new StringBuilder();

        s.append("application{name=" + name + ", ");

        // browsing sources
        s.append("Sources[");
        for (Source a : sources) {
            s.append(a.toString());
            s.append(", ");
        }

        s.append("]");

        s.append("}");

        return s.toString();
    }

}
