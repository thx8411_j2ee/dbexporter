package fr.fam.dbexporter.config;

/**
*
*/
public class Source {
    /** */
    static final int DEFAULT_TIMEOUT = 5000;

    /** */
    static final double DEFAULT_VALUE = 0.0;

    /** */
    private String url;

    /** */
    private int timeout = DEFAULT_TIMEOUT;

    /** */
    private String login;

    /** */
    private String password;

    /** */
    private String env;

    /** */
    private double value = DEFAULT_VALUE;
    
    /**
    *
    * @return url
    */
    public final String getUrl() {
        return url;
    }

    /**
    *
    * @param purl application url
    */
    public final void setUrl(final String url) {
        this.url = url;
    }

    /**
    *
    * @return timeout
    */
    public final int getTimeout() {
        return timeout;
    }

    /**
    *
    * @param ptimeout timeout
    */
    public final void setTimeout(final int timeout) {
        this.timeout = timeout;
    }

    /**
    *
    * @return login
    */
    public final String getLogin() {
        return login;
    }

    /**
    *
    * @param plogin application login
    */
    public final void setLogin(final String login) {
        this.login = login;
    }

    /**
    *
    * @return password
    */
    public final String getPassword() {
        return password;
    }

    /**
    *
    * @param ppassword application password
    */
    public final void setPassword(final String password) {
        this.password = password;
    }

    /**
    *
    * @return env
    */
    public final String getEnv() {
        return env;
    }

    /**
    *
    * @param penv source env
    */
    public final void setEnv(String env) {
        this.env = env;
    }

   /**
    *
    * @return value
    */
    public final double getValue() {
        return value;
    }

    /**
    *
    * @param pvalue source value
    */
    public final void setValue(double value) {
        this.value = value;
    }
    

    /**
    *
    * @return string
    */
    @Override
    public final String toString() {
        StringBuilder s = new StringBuilder();

        s.append("url=" + url + ", ");
        s.append("timeout=" + timeout + ", ");
        s.append("login=" + login + ", ");
        s.append("password=" + "*******" + ", ");
        s.append("env=" + env);

        return s.toString();
    }
}
