package fr.fam.dbexporter;

    import java.sql.*;  
    class MysqlCon{  

	private String info ;
	private String state ;

	public MysqlCon(){
	this.info = "none";
	this.state = "KO";
	}

    	public void main(){  
    		try{  
    			Class.forName("com.mysql.jdbc.Driver");
    			Connection con=DriverManager.getConnection(  
    			"jdbc:mysql://localhost:3306/baseTest","test","test");  
    			//here maBase is database name, test is username and password  
    			DatabaseMetaData meta = con.getMetaData();
    			ResultSet rs = meta.getTableTypes();
			rs.beforeFirst();  
			this.info = "";    			
			while(rs.next()){
			this.info += rs.getString(1)+" ";}				    
    			con.close();  
			if (this.info != ""){this.state = "OK";}
			else if (this.info.equals("")){this.state = "KO";}
    			}catch(Exception e){ this.info ="Erreur : "+e;}  

    	}

	public String getInfo(){
        return this.info ;
	}
	public String getState(){return this.state;} 
  
    }  
