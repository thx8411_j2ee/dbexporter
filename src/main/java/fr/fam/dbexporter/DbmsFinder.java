
package fr.fam.dbexporter;

public class DbmsFinder {
	/**
	 * 
	 * @param pUrl
	 * @return dbms  the dbms name of the database
	 */
	public static String getDbms(final String pUrl ){
		String[] url = pUrl.split("/");
		String[] dbinfo = url[0].split(":");
		String dbms = "";
		for (int i = 0; i<dbinfo.length;i++) {
			
			if(dbinfo[i].equals("mysql") || dbinfo[i].equals("postgresql") || dbinfo[i].equals("oracle") || dbinfo[i].equals("sqlserver") || dbinfo[i].equals("sybase")) {
			  dbms = dbinfo[i];
			}
		}
		return dbms;
}
}
