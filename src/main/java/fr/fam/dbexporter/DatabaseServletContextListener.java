
package fr.fam.dbexporter;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import fr.fam.dbexporter.config.DatabaseConfig;

/**
*
*/
@WebListener
public class DatabaseServletContextListener implements ServletContextListener {

    /** */
    private static final Logger LOGGER = Logger.getLogger(DatabaseServletContextListener.class);

    
   public final void contextInitialized(final ServletContextEvent sce) {

        DatabaseConfig config = new DatabaseConfig();
        DatabaseCollector collector = new DatabaseCollector(config);

        ServletContext sc = sce.getServletContext();
        sc.setAttribute("collector", collector);

        LOGGER.info("ServletContextListener started");
    }

    
    public final void contextDestroyed(final ServletContextEvent sce) {

        ServletContext sc = sce.getServletContext();
        DatabaseCollector collector = (DatabaseCollector) sc.getAttribute("collector");
        collector.close();
        sc.removeAttribute("collector");

        LOGGER.info("ServletContextListener destroyed");
    }
}
