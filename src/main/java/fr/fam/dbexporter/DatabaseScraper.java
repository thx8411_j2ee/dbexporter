
package fr.fam.dbexporter;

import java.sql.*;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import fr.fam.dbexporter.config.Source;



public class DatabaseScraper implements Callable<Void> {

	private static final Logger LOGGER = Logger.getLogger(DatabaseScraper.class);

	private Source source;

	public DatabaseScraper(final Source pSource) {
		source = pSource;
	}
    /**
     * 
     */
	public final Void call() {
		LOGGER.debug("scraping source : " + source.toString());
		
		
		checkDatabase();
		
		return null;
	}

	public void checkDatabase() {
		// MySQL database checking
		if (DbmsFinder.getDbms(source.getUrl()).equals("mysql") ) {
			String probeInfo = "";
			try {
				Class.forName("com.mysql.jdbc.Driver");
				Connection con = DriverManager.getConnection(source.getUrl(), source.getLogin(), source.getPassword());


				DatabaseMetaData meta = con.getMetaData();
				ResultSet rs = meta.getTableTypes();
				rs.beforeFirst();
				
				
				
				while (rs.next()) {
					probeInfo += rs.getString(1) + "";
				}
				con.close();
				
				
				if (probeInfo != "" ) {
					probeInfo = "OK";
					LOGGER.debug("status : "+probeInfo);
					source.setValue(100.0);
				} 
				else if (probeInfo.equals("") || probeInfo.equals("null")) {
					probeInfo = "KO";
					LOGGER.debug("status : "+probeInfo);
					source.setValue(0.0);
				}
			} catch (Exception e) {
				probeInfo = "KO";
				LOGGER.debug("status : "+probeInfo);
				LOGGER.error(e.toString());
				source.setValue(0.0);
			}
			
			
		}//if end for the mysql case
		
		// postgresql database checking
		if (DbmsFinder.getDbms(source.getUrl()).equals("postgresql") ) {
			String probeInfo2 = "";
			try {
				Class.forName("org.postgresql.Driver");
				Connection con = DriverManager.getConnection(source.getUrl(), source.getLogin(), source.getPassword());


				DatabaseMetaData meta = con.getMetaData();
				ResultSet rs = meta.getTableTypes();
				rs.beforeFirst();
				
				
				
				while (rs.next()) {
					probeInfo2 += rs.getString(1) + "";
				}
				
				con.close();
				
				if (probeInfo2 != "" ) {
					probeInfo2 = "OK";
					LOGGER.debug("status : "+probeInfo2);
					source.setValue(100.0);
				} 
				else if (probeInfo2.equals("") || probeInfo2.equals("null") ) {
					probeInfo2 = "KO";
					LOGGER.debug("status : "+probeInfo2);
					source.setValue(0.0);
				}
				
			} catch (Exception e) {
				probeInfo2 = "KO";
				LOGGER.debug("status : "+probeInfo2);
				LOGGER.error(e.toString());
				source.setValue(0.0);
			}
			
			
		}//if end for the postgresql case
		
		// Oracle database checking
		if (DbmsFinder.getDbms(source.getUrl()).equals("oracle") ) {
			String probeInfo2 = "";
			try {
				Class.forName("oracle.jdbc.OracleDriver");
				Connection con = DriverManager.getConnection(source.getUrl(), source.getLogin(), source.getPassword());


				DatabaseMetaData meta = con.getMetaData();
				ResultSet rs = meta.getTableTypes();
				rs.beforeFirst();
				
				
				
				while (rs.next()) {
					probeInfo2 += rs.getString(1) + "";
				}
				
				con.close();
				
				if (probeInfo2 != "" ) {
					probeInfo2 = "OK";
					LOGGER.debug("status : "+probeInfo2);
					source.setValue(100.0);
				} 
				else if (probeInfo2.equals("") || probeInfo2.equals("null") ) {
					probeInfo2 = "KO";
					LOGGER.debug("status : "+probeInfo2);
					source.setValue(0.0);
				}
				
			} catch (Exception e) {
				probeInfo2 = "KO";
				LOGGER.debug("status : "+probeInfo2);
				LOGGER.error(e.toString());
				source.setValue(0.0);
			}
			
			
		}//if end for the oracle case
		
		// Sybase database checking
		if (DbmsFinder.getDbms(source.getUrl()).equals("sybase") ) {
			String probeInfo2 = "";
			try {
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
				Connection con = DriverManager.getConnection(source.getUrl(), source.getLogin(), source.getPassword());


				DatabaseMetaData meta = con.getMetaData();
				ResultSet rs = meta.getTableTypes();
				rs.beforeFirst();
				
				
				
				while (rs.next()) {
					probeInfo2 += rs.getString(1) + "";
				}
				
				con.close();
				
				if (probeInfo2 != "" ) {
					probeInfo2 = "OK";
					LOGGER.debug("status : "+probeInfo2);
					source.setValue(100.0);
				} 
				else if (probeInfo2.equals("") || probeInfo2.equals("null") ) {
					probeInfo2 = "KO";
					LOGGER.debug("status : "+probeInfo2);
					source.setValue(0.0);
				}
				
			} catch (Exception e) {
				probeInfo2 = "KO";
				LOGGER.debug("status : "+probeInfo2);
				LOGGER.error(e.toString());
				source.setValue(0.0);
			}
			
			
		}//if end for the sybase case

		// SQL-Server database checking
		if (DbmsFinder.getDbms(source.getUrl()).equals("sqlserver") ) {
			String probeInfo2 = "";
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				String urlCon = source.getUrl()+";user="+source.getLogin()+";password="+source.getPassword();
				Connection con = DriverManager.getConnection(urlCon);


				DatabaseMetaData meta = con.getMetaData();
				ResultSet rs = meta.getTableTypes();
				rs.beforeFirst();
				
				
				
				while (rs.next()) {
					probeInfo2 += rs.getString(1) + "";
				}
				
				con.close();
				
				if (probeInfo2 != "" ) {
					probeInfo2 = "OK";
					LOGGER.debug("status : "+probeInfo2);
					source.setValue(100.0);
				} 
				else if (probeInfo2.equals("") || probeInfo2.equals("null") ) {
					probeInfo2 = "KO";
					LOGGER.debug("status : "+probeInfo2);
					source.setValue(0.0);
				}
				
			} catch (Exception e) {
				probeInfo2 = "KO";
				LOGGER.debug("status : "+probeInfo2);
				LOGGER.error(e.toString());
				source.setValue(0.0);
			}
			
			
		}//if end for the sqlserver case

		
	}

}
